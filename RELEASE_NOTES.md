Release Notes
-------------
2.1.0
-----
* Added a new common interface for dealing with formatters which need to return a Map instead of a Vector.

2.0.0
-----
* Fixed FormatException namespace.
* [BC Break] Rewrote interface to be more flexible.

1.0.0
-----
* Initial Release.
