<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\format\driver\common
{
	interface CommonInterface
	{
		public function read(string $filename, Map<arraykey,mixed> $options=Map{}):Vector<Map<string,mixed>>;
		public function write(string $filename, Vector<mixed> $data, Map<arraykey,mixed> $options=Map{}):bool;
	}
}